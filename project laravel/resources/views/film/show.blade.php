@extends('layout.master')
@section('judul')
Halaman Detail Film {{$film->judul}}
@endsection    
@section('content')

<img src="{{asset('gambar/'.$film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>

<h1>Kritik</h1>

@foreach ($film->kritik as $item)
    <div class="card">
        <div class="card-body">
          <small><b>{{$item->user->name}}</b></small>
          <p class="card-text">{{$item->content}}</p>
          <p class="card-text">{{$item->point}}</p>
        </div>
    </div>
@endforeach
<form action="/kritik" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf
    <div class="form-group">
      <label>Content</label>
      <input type="hidden" name="film_id" value="{{$film->id}}">
      <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Point</label>
        <input type="number" name='point' class="form-control">
      </div>
      @error('point')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Kembali</a>

@endsection