<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-tables', 'IndexController@data_tables');


// CRUD Cast
// Create
Route::get('/cast/create', 'CastController@create'); // route menuju form create
Route::post('/cast', 'CastController@store'); // route untuk menyimpan data ke database

// Read
Route::get('/cast', 'CastController@index'); // route list cast
Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju ke form edit
Route::put('/cast/{cast_id}', 'CastController@update'); // route untuk update data berdasarkan di database

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route untuk hapus data di database


Route::group(['middleware' => ['auth']], function() {
    // CRUD Genre
    // Create
    Route::get('/genre/create', 'GenreController@create'); // route menuju form create
    Route::post('/genre', 'GenreController@store'); // route untuk menyimpan data ke database
    
    // Read
    Route::get('/genre', 'GenreController@index'); // route list cast
    Route::get('/genre/{genre_id}', 'GenreController@show'); // route detail genre
    
    // Update
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit'); // route menuju ke form edit
    Route::put('/genre/{genre_id}', 'GenreController@update'); // route untuk update data berdasarkan di database
    
    // Delete
    Route::delete('/genre/{genre_id}', 'GenreController@destroy'); // route untuk hapus data di database

    // Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('kritik', 'KritikController')->only([
        'index', 'store'
    ]);

    Route::resource('peran', 'PeranController')->only([
        'index', 'store'
    ]);
});




// CRUD Film
Route::resource('film', 'FilmController');
Auth::routes();

